# Automating xxx API Endpoints

API automation using Serenity and Cucumber

## 📦 Install

###Pre-requisite:
Java

Gradle

```sh
gradle build
```


## ✅ Test

```sh
gradle cucumber
```
